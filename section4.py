# excersice1 about truthy and falsy
# false is {},(),[],set(),None,'',0,0.0,0j

is_age = 0.0 # change value ex is_age = 0,[],(),{},set(),0.0
is_dob = '21/4/2019'

if is_dob and is_age:  # hear not check value but look like this "if bool(is_dob) and bool(is_dob):"
    print('Both are exits')
else:
    print('Someone is missing')


# excersice2 about ternary operator

is_friend1 = is_friend2 = True
message = 'is allowed to message' if is_friend1 and is_friend2 else 'is not allowed to message'
print(message)






