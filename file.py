# exersice1
'''
username = input('Enter username:- ')
password = input('Enter password:- ')
print(f'Heyy {username}, Your password is {"*"*len(password)} ')'''

#exersice2
'''
print('***' * 2)
print('\'smit\'\n' * 10)
print('\'' * 2)
print('hell'+('o'*10))'''

#exersice3
'''
amazon_cart = ['1','2']
flipkart_cart = amazon_cart
flipkart_cart[0] = '0'
print(amazon_cart[0])'''

#exersice4 about List
'''
#What is the output of this code?
#Before you clikc RUN, guess the output of each print statement!
new_list = ['a', 'b', 'c']
print(new_list[1]) # b
print(new_list[-2]) # b 
print(new_list[1:3]) # b c
new_list[0] = 'z' 
print(new_list) # z b c

my_list = [1,2,3] 
bonus = my_list + [5]
my_list[0] = 'z'
print(my_list) # z 2 3
print(bonus) # 1 2 3 5'''


#exersice5 about packing and unpacking
'''
# list unpacking
a,b,c = [1,2,3]

print(a)
print(b)
print(c)
print("Total :",a+b+c)

#this is new things
a,b,*other = ['a','b','c','d','e']  # if i want only two values for two variables we have to store rest of the list into another variable uing star sign
print(a)
print(b)
print("Rest of the list :",other)

#we can also assign a variable and unpacking of list like hear
a, b, *other, d = ['a','b','c','d','e']
print(other)
print(d)
'''

#excersice6 about None datatype
'''
a = None # None is special datatypes to assign value 'None' means it has nothing
print(type(a))
print(a)
'''

#excersice7 about dictionary
'''
#dictionary

dictionary = {
    'b' : 1,
    True : 2
}

print(dictionary[True])
'''
'''
emptydict1 = {}
emptydict2 = dict()
print(emptydict1,emptydict2)

#another way to create dictionary
user1 = dict(name='SmitBhikadiya')  # but this another way
print(user1)

# method of dictionary

dictionary = {
    'a' : 100,
    'b' : 200
}

print(dictionary.get('a')) 
print(dictionary.get('c'))  # get method is accept exception in-built
print(dictionary.get('a',100)) # we can also assign default value of key if key is not exits than output will be default

dict1 = dictionary.copy()
print(dict1)
dict1.clear()
print(dict1)

dictionary.update({'b':300})
print(dictionary)
dictionary.update({'c':200})
print(dictionary)

print(dictionary.pop('b'))
print(dictionary.popitem()) # this will remove random key value pair
print(dictionary)

dictionary.update({'a':100,'b':200,'c':300})
print(dictionary)

print(dictionary.items())
print(dictionary.values())
print(dictionary.keys())

#check key is exit or not

print('a' in dictionary)
print(100 in dictionary.values())
'''

#excersice8 about tuple
'''Tuple is immutable for storing data safe that's why we cant change key of dictionary'''

'''
t1 = tuple()
t2 = (1,) # t2 = (1) this will be integer type so we have to add comma at last when we have only one value in tuple
print(type(t1),type(t2))

t3 = (1,2,3,4,5,6)
print(t3[0:])
print(t3[::-1])

#we cant change value of tuple it will give you error but i can 
l1 = list(t3)
l1[0] = 10
t3 = tuple(l1)

print(t3)

# tuple has only two method
print(t3.count(5))
print(t3.index(10))
'''

# excwesice9 about sets
'''
sets are unorderd data strucutre, using for arithmatic operation like intersaction, union, diffrence between the multiple set
'''
'''
set1 = {1,2,3,4,5,6,7,8,9,10}
set2 = {2,4,6,8,10}

print(set1 - set2)
print(set1.difference(set2))

print(set1 | set2) # find union
print(set1.union(set2)) # find union

print(set1 & set2) # find intersaction
print(set1.intersection(set2)) # find intersaction

print(set1.isdisjoint(set2)) # check relation between sets if any relation are there it will return false
print(set1.issubset(set2))
print(set1.issuperset(set2))

s1 = {1,True,'smit',(1,2,3)} # we cant add list or dict
print(s1) # we can see in output True are not printed because True = 1 so set not having any duplicate value therefore set remove it

s1.add('smit_bhikadiya')
s1.pop()  # this method remove random data into the sets
print(s1)

s1.clear()
print(s1)
print(type({1,}))
'''



